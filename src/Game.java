import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Game {
    private Card[] cards;

    public Game(int nbPaire) {
        if(!(nbPaire >= 3 && nbPaire <= 20)) {
            throw new IllegalArgumentException("Le nombre de paires n’est pas entre 3 et 20 inclus");
        }

        cards = new Card[2*nbPaire];
        for (int i = 0 ; i < cards.length ; i++) {
            cards[i] = new Card(i%nbPaire);
        }
    }

    public void shuffle(){
        List<Card> listCard = Arrays.asList(cards);
        Collections.shuffle(listCard);
        cards = listCard.stream().toArray(n -> new Card[n]);
    }

    private int getSize(){
        return cards.length;
    }

    private boolean isHidden(int pos){
        return cards[pos].isHidden();
    }

    public int getCardValue(int pos){
        return cards[pos].getValue();
    }

    public boolean checkPositions(int pos1, int pos2){
        if(pos1 == pos2){
            throw new IllegalArgumentException("Vous devez séléctionner 2 carte différente");
        }

        boolean cardIdentique = this.getCardValue(pos1) == this.getCardValue(pos2);
        if(cardIdentique){
            cards[pos1].reveal();
            cards[pos2].reveal();
        }
        return cardIdentique;
    }

    public boolean isGameOver() {
       int cpt = 0;
       boolean isGameOver = true;
       while (cpt < this.getSize() && isGameOver){
           if(this.isHidden(cpt)) {
               isGameOver = false;
           }
           cpt++;
       }
       return isGameOver;
    }

    @Override
    public String toString() {
        String value = "";
        int defaultNbSpace = 4;
        for (int i = 0; i < this.cards.length; i++){
            value += printSpace(defaultNbSpace , String.valueOf(i).length()) + i;
        }
        value += "\n";
        for (Card card: this.cards){
            value += printSpace(defaultNbSpace , card.toString().length()) + card.toString();
        }
        return value;
    }

    private String printSpace(int defaultNbSpace, int length){
        String spaces = "";
        for (int i = 0; i < defaultNbSpace - length; i++){
            spaces += " ";
        }
        return spaces;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Arrays.equals(cards, game.cards);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(cards);
    }
}
