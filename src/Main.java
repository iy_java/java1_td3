import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        System.out.println("Nombre de paires ?");
        int nbPaire = clavier.nextInt();
        Memory memory = new Memory(nbPaire);
        int nbTours = memory.playMemory();
        System.out.println("Vous avez gagné en " + nbTours + " tours");
    }
}