import java.util.Scanner;

public class Memory {
    private Game game;


    public Memory(int nbPaire) {
        this.game = new Game(nbPaire);
    }

    private int askPosition() {
        Scanner clavier = new Scanner(System.in);
        System.out.println("Quel carte voulez vous retourné");
        return clavier.nextInt();
    }

    public int playMemory(){
        int coup1, coup2, nbTours = 0;
        this.game.shuffle();
        while (!this.game.isGameOver()){
            System.out.println("---------------------------------------");
            System.out.println("Affichage carte");
            System.out.println("---------------------------------------");
            System.out.println(this.game.toString());
            coup1 = askPosition();
            System.out.println(this.game.getCardValue(coup1));
            coup2 = askPosition();
            System.out.println(this.game.getCardValue(coup2));
            this.game.checkPositions(coup1, coup2);
            nbTours++;
        }
        System.out.println(this.game.toString());
        return nbTours;
    }
}
