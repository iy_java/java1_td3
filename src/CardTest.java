import static org.junit.jupiter.api.Assertions.*;

class CardTest {

    @org.junit.jupiter.api.Test
    void getValue() {
        int expectedValue = 5;
        Card card = new Card(expectedValue);
        assertEquals(card.getValue(), expectedValue);
    }

    @org.junit.jupiter.api.Test
    void isHidden() {
        Card card = new Card(5);
        assertTrue(card.isHidden());
    }

    @org.junit.jupiter.api.Test
    void reveal() {
        Card card = new Card(5);
        card.reveal();
        assertFalse(card.isHidden());
    }

    @org.junit.jupiter.api.Test
    void testToString() {
        Card card = new Card(5);
        assertEquals(card.toString(), "?");
        card.reveal();
        assertEquals(card.toString(), "5");
    }

    @org.junit.jupiter.api.Test
    void testEquals1() {
        Card card1 = new Card(5);
        Card card2 = new Card(5);
        assertTrue(card1.equals(card2));
    }

    @org.junit.jupiter.api.Test
    void testEquals2() {
        Card card1 = new Card(5);
        Card card2 = new Card(6);
        assertFalse(card1.equals(card2));
    }
}