import java.util.Objects;

public class Card {
    private int value;
    private boolean hidden;

    public Card(int value) {

        if(value < 0){
            throw new IllegalArgumentException("La valeur ne peut pas être négatif");
        }

        this.value = value;
        this.hidden = true;
    }

    public int getValue() {
        return value;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void reveal(){
        this.hidden = false;
    }

    @Override
    public String toString() {
        String chaine = "?";
        if(!this.hidden){
            chaine = String.valueOf(this.value);
        }
        return chaine;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return value == card.value &&
                hidden == card.hidden;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, hidden);
    }
}
